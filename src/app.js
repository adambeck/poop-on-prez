/*
 * Importing an html file doesn't actually do anything with the contents but allows us to
 * make modifications to the file and have the webpack-dev-server automatically reload the page
 * for us. Yes, it seems a bit hacky; so, in the future this problem may be solved in a
 * much cleaner way. Be on the look out.
 *
 * http://stackoverflow.com/questions/33183931/how-to-watch-index-html-using-webpack-dev-server-and-html-webpack-plugin
 */
if (NODE_ENV === 'development') {
  require('../public/index.html');
}

import './main.css';
import imageMapResizer from 'image-map-resizer';
import io from 'socket.io-client';

imageMapResizer();
const socket = io();

const TRUMP_ID = 0;
const CLINTON_ID = 1;

const main = document.getElementsByTagName('main')[0];

const createPoop = (parent, x, y) => {
  const img = document.createElement('img');
  img.src = `/images/poop-1.png`;
  img.width = '30';
  img.height = '30';
  img.style.left = x - (30 / 2) + 'px';
  img.style.top = y - (30 / 2) + 'px';
  img.classList.add('poop');
  img.draggable = false;
  parent.appendChild(img);
};

const imageClickHandler = imageId => {
  const parent = imageId === TRUMP_ID ? document.querySelector('.trump') : document.querySelector('.clinton');
  return event => {
    createPoop(parent, event.offsetX, event.offsetY);
    socket.emit('new poop', {imageId});
  }
};

var trumpCounter = new flipCounter('trumpCounter', {value: 0, auto: false});
var clintonCounter = new flipCounter('clintonCounter', {value: 0, auto: false});
var trumpCounterSm = new flipCounter('trumpCounterSm', {value: 0, auto: false});
var clintonCounterSm = new flipCounter('clintonCounterSm', {value: 0, auto: false});

const trump = document.querySelector('div.trump');
const trumpWipe = document.getElementById('trumpWipe');
const trumpMap = document.getElementById('trump-map');
const trumpClickHandler = imageClickHandler(TRUMP_ID);
trumpMap.addEventListener('click', (e) => {
  e.preventDefault();
  trumpClickHandler(e);
});

trumpWipe.addEventListener('click', () => {
  Array.from(trump.querySelectorAll('.poop')).forEach(poop => {
    poop.remove();
  });
});

const clinton = document.querySelector('div.clinton');
const clintonWipe = document.getElementById('clintonWipe');
const clintonMap = document.getElementById('clinton-map');
const clintonClickHandler = imageClickHandler(CLINTON_ID);
clintonMap.addEventListener('click', (e) => {
  e.preventDefault();
  clintonClickHandler(e);
});

clintonWipe.addEventListener('click', () => {
  Array.from(clinton.querySelectorAll('.poop')).forEach(poop => {
    poop.remove();
  });
});

socket.on('poop received', data => {
  if (data.trumpPoops) {
    trumpCounter.setValue(data.trumpPoops);
    trumpCounterSm.setValue(data.trumpPoops);

  }

  if (data.clintonPoops) {
    clintonCounter.setValue(data.clintonPoops);
    clintonCounterSm.setValue(data.clintonPoops);
  }
});




// var myCounter = document.getElementById('myCounter');
