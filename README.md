# Webpack Static Site

A boilerplate for a static site with webpack. Includes a Hapi server.


## Getting Started

There are 3 moving parts to this development environment - an nginx server, a hapi server, and the webpack development server.

Run `./start_node.sh` from the project's root directory.
Run `./start_nginx.sh` from the project's root directory.
Run `node dev-server.js` from the project's root directory.

The nginx server port must be exposed to the host so that the webpack dev server can access it.

You should now have access to the whole server by accessing `localhost:4040`.

## Default Ports

- Webpack Dev Server `:4040`
- Node Application `:4050`
- Nginx `:3005`
