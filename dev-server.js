var Webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');

var config = require('./webpack.config.js');
config.entry.unshift('webpack-dev-server/client?http://0.0.0.0:4040/');
config.entry.unshift('webpack/hot/dev-server');

var compiler = Webpack(config);

var bundler = new WebpackDevServer(compiler, {
  proxy: {
    '/*': {
      target: 'http://localhost:3005',
      secure: false
    }
  },
  contentBase: 'public/',
  publicPath: '/',
  hot: true
});

bundler.listen(4040, function() {
  console.log('listening...');
});
