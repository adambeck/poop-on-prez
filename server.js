const Hapi = require('hapi');
const server = new Hapi.Server();
const redis = require('redis');
const client = redis.createClient({ host: 'db' });

client.setnx('trump_poops', '0');
client.setnx('clinton_poops', '0');

const TRUMP_ID = 0;
const CLINTON_ID = 1;

server.connection({
  host: '0.0.0.0',
  port: '80'
});

const io = require('socket.io')(server.listener);

io.on('connection', socket => {

  client.get('trump_poops', (err, trump_poops) => {
    if (err) {
      throw err;
    }

    client.get('clinton_poops', (err, clinton_poops) => {
      if (err) {
        throw err;
      }

      io.emit('poop received', {
        trumpPoops: parseInt(trump_poops, 10),
        clintonPoops: parseInt(clinton_poops, 10)
      });

    });
  });

  socket.on('new poop', data => {

    if (data.imageId === TRUMP_ID) {
      client.incr('trump_poops', (err, reply) => {
        io.emit('poop received', {
          trumpPoops: parseInt(reply, 10)
        });
      });
    }

    if (data.imageId === CLINTON_ID) {
      client.incr('clinton_poops', (err, reply) => {
        io.emit('poop received', {
          clintonPoops: parseInt(reply, 10)
        });
      });
    }
  });
});

server.start(err => {

  if (err) {
    throw err;
  }

  console.log(`Server running at: ${server.info.uri}`);

});
